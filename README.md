# mnist-rs

The MNIST dataset contains a training set of 60,000 images of handwritten digits. This dataset was focused mainly for Python developer. This dataset is a replica with the same structure for developers in rust.

- 20 images of 28x28 pixels
- pretrained model available
- PR for more data is open

Run the pretrained model:

```bash
cargo run --features pretrained
```

Compile the model:

```bash
cargo run
```

<div align="center">

![Math](https://gitlab.com/mateolafalce/mnist-rs/-/raw/main/model.png?ref_type=heads)

</div>