use crate::{
    model::{ATTENTION, SIZE_IMG},
    PIXELS,
};
use image::{GenericImageView, Pixel};
use std::{cmp::Ordering, collections::HashMap, f32::consts::E};

pub fn relu(pixel: f32) -> f32 {
    if pixel < ATTENTION {
        0.0
    } else {
        pixel
    }
}

pub fn softmax(values: [f32; PIXELS]) -> [f32; PIXELS] {
    let e: f32 = E;
    let exp_values: Vec<f32> = values.iter().map(|&v| e.powf(v)).collect();
    let sum_exp_values: f32 = exp_values.iter().sum();
    exp_values
        .iter()
        .map(|&v| v / sum_exp_values)
        .collect::<Vec<_>>()
        .try_into()
        .unwrap()
}

pub fn closest_number_index(arr: &[f32], x: f32) -> Option<usize> {
    arr.iter()
        .enumerate()
        .min_by(|&(_, a), &(_, b)| {
            let dist_a = (a - x).abs();
            let dist_b = (b - x).abs();
            dist_a.partial_cmp(&dist_b).unwrap_or(Ordering::Equal)
        })
        .map(|(index, _)| index)
}

pub fn most_frequent_number(numbers: &[usize]) -> Option<usize> {
    let mut frequency_map = HashMap::new();
    for &num in numbers {
        *frequency_map.entry(num).or_insert(0) += 1;
    }
    frequency_map
        .into_iter()
        .max_by_key(|&(_, count)| count)
        .map(|(num, _)| num)
}

pub fn av(number: [f32; 10], sum: f32) -> [f32; 10] {
    let mut number_av = number;
    for num in &mut number_av {
        *num /= sum
    }
    number_av
}

pub fn get_image(path: &str) -> [f32; SIZE_IMG] {
    let mut input_layer: [f32; SIZE_IMG] = [0.0; SIZE_IMG];
    let img = image::open(path).unwrap();
    let mut input_iter = 0;
    for i in 0..PIXELS {
        for j in 0..PIXELS {
            let pixel_rgba: image::Rgba<u8> = img.get_pixel(i as u32, j as u32);
            let pixel = pixel_rgba.to_rgb();
            let sum_color: u16 =
                (255 - pixel[0]) as u16 + (255 - pixel[1]) as u16 + (255 - pixel[2]) as u16;
            let av_color: f32 = sum_color as f32 / 765.0;
            let black_pixel = relu(av_color);
            input_layer[input_iter] = black_pixel;
            input_iter += 1;
        }
    }
    input_layer
}
