use crate::functions::{get_image, softmax};

/// pixels size of images
pub const PIXELS: usize = 28;
/// pixel size image = PIXELS * PIXELS
pub const SIZE_IMG: usize = 784;
/// a strong grey is consider part of the number
/// else bellow is just sound.
pub const ATTENTION: f32 = 0.004;
/// [f32; 28] -> Hidden layer, 28 neurons
pub type HidenLayer = [f32; PIXELS];
/// How much rows of data we have
const AMOUNT_DATA: usize = 2;

pub fn train_model() -> [HidenLayer; 10] {
    let mut saved_state: [HidenLayer; 10] = [[0.0; PIXELS]; 10];
    for amount_data in 1..AMOUNT_DATA + 1 {
        //for training in 0..10 {
        for (training, state) in saved_state.iter_mut().enumerate() {
            // 28 * 28 input layer
            let path = "dataset/train/".to_owned()
                + &training.to_string()
                + "_"
                + &amount_data.to_string()
                + ".png";
            let input_layer: [f32; SIZE_IMG] = get_image(&path);

            // PIXELS hidden layers
            let mut input_iter = 0;
            let mut hidden_layer: HidenLayer = [0.0; PIXELS];
            for hidden in hidden_layer.iter_mut().take(PIXELS) {
                let mut row: f32 = 0.0;
                for _ in 0..PIXELS {
                    row += input_layer[input_iter];
                    input_iter += 1;
                }
                *hidden = row
            }
            let softmax = softmax(hidden_layer);
            for (i, softmax) in softmax.iter().enumerate().take(PIXELS) {
                state[i] += softmax;
            }
        }
    }
    for saved in &mut saved_state {
        for state in saved.iter_mut().take(PIXELS) {
            *state /= AMOUNT_DATA as f32;
        }
    }
    saved_state
}
