use serde::{Deserialize, Serialize};
#[cfg(feature = "pretrained")]
use serde_json::from_str;
#[cfg(feature = "pretrained")]
use std::{fs::File, io::Read};

pub mod functions;
pub mod model;
pub mod test_model;

#[cfg(not(feature = "pretrained"))]
use crate::{
    model::{train_model, HidenLayer, PIXELS},
    test_model::test_model,
};
#[cfg(feature = "pretrained")]
use crate::{
    model::{HidenLayer, PIXELS},
    test_model::test_model,
};

#[derive(Serialize, Deserialize, Debug)]
struct Layers {
    state: [HidenLayer; 10],
}

fn main() {
    // pre-trained model
    #[cfg(feature = "pretrained")]
    {
        let mut file = File::open("model.json").unwrap();
        let mut json_string = String::new();
        file.read_to_string(&mut json_string).unwrap();
        let deserialized: Layers = from_str(&json_string).unwrap();
        test_model(deserialized.state);
    }
    // compile the model
    #[cfg(not(feature = "pretrained"))]
    {
        let saved_state: [HidenLayer; 10] = train_model();
        test_model(saved_state);
    }
}
