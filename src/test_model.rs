use crate::{
    functions::{closest_number_index, get_image, softmax},
    model::{HidenLayer, SIZE_IMG},
    PIXELS,
};

pub fn test_model(saved_state: [HidenLayer; 10]) {
    for testing in 0..1 {
        let path = "dataset/test/".to_owned() + &testing.to_string() + "_1.png";
        let input_layer: [f32; SIZE_IMG] = get_image(&path);
        // PIXELS hidden layers
        let mut input_iter: usize = 0;
        let mut hidden_layer: HidenLayer = [0.0; PIXELS];
        for hidden in hidden_layer.iter_mut().take(PIXELS) {
            let mut row: f32 = 0.0;
            for _ in 0..PIXELS {
                row += input_layer[input_iter];
                input_iter += 1;
            }
            *hidden = row
        }
        hidden_layer = softmax(hidden_layer);
        let mut result: [f32; 10] = [0.0; 10];
        for i in 0..10 {
            let mut number: f32 = 0.0;
            for (j, hidden) in hidden_layer.iter().enumerate().take(PIXELS - 1).skip(1) {
                number += (hidden - saved_state[i][j]).abs();
                result[i] = number;
            }
            result[i] = number;
        }
        let index: usize = closest_number_index(&result, 0.0).unwrap();
        println!("[{}] -> {}", testing, index);
    }
}
